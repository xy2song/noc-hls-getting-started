#include "/opt/Xilinx/Vivado/2018.3/include/gmp.h"  //cosim workaround for know Vivado HLS bug
#include "ap_int.h"
#include <stdio.h>
#include "top.h"
#include "hls_stream.h"

void noc_ring (STREAM_T stream_in[NUM_ROUTERS],
               STREAM_T stream_out[NUM_ROUTERS],
               ap_uint<MAX_NUM_BITS_VEC_INDEX> indir_vec[NUM_ROUTERS][MAX_INDIR_VECTOR_SIZE], 
               ap_uint<NUM_BITS_ROUTER_ADDR> route_vec[NUM_ROUTERS][MAX_INDIR_VECTOR_SIZE] ) {

    // routers inputs
    static NOC_PKT up_wires_rd[NUM_ROUTERS];
    static NOC_PKT down_wires_rd[NUM_ROUTERS];
    static NOC_PKT left_wires_rd[NUM_ROUTERS];
    #pragma HLS data_pack variable=up_wires_rd
    #pragma HLS data_pack variable=down_wires_rd
    #pragma HLS data_pack variable=left_wires_rd
    static bool up_vld_rd[NUM_ROUTERS]={0};
    static bool down_vld_rd[NUM_ROUTERS]={0};
    static bool left_vld_rd[NUM_ROUTERS]={0};

    // routers outputs
    NOC_PKT up_wires_wr[NUM_ROUTERS];
    NOC_PKT down_wires_wr[NUM_ROUTERS];
    NOC_PKT left_wires_wr[NUM_ROUTERS];
    NOC_PKT right_wires_wr[NUM_ROUTERS];
    #pragma HLS data_pack variable=up_wires_wr
    #pragma HLS data_pack variable=down_wires_wr
    #pragma HLS data_pack variable=left_wires_wr
    #pragma HLS data_pack variable=right_wires_wr
    bool up_vld_wr[NUM_ROUTERS]={0};
    bool down_vld_wr[NUM_ROUTERS]={0};
    bool left_vld_wr[NUM_ROUTERS]={0};
    bool right_vld_wr[NUM_ROUTERS]={0};

    // sender inputs
    static NOC_PKT sender_in[NUM_ROUTERS];
    #pragma HLS data_pack variable=sender_in
    static bool sender_in_vld[NUM_ROUTERS]={0};

    // sender outputs
    NOC_PKT sender_out[NUM_ROUTERS];
    #pragma HLS data_pack variable=sender_out
    bool sender_out_vld[NUM_ROUTERS]={0};

    // recevier inputs
    static NOC_PKT receiver_in[NUM_ROUTERS];
    #pragma HLS data_pack variable=receiver_in
    static bool receiver_in_vld[NUM_ROUTERS]={0};

    static ap_uint<1> east_prev_win[NUM_ROUTERS];
    static ap_uint<1> north_prev_win[NUM_ROUTERS];
    static ap_uint<1> south_prev_win[NUM_ROUTERS];

    INSTANCES: for(int k=0; k<NUM_ROUTERS; k++) {
        sender(stream_in[k], 
               indir_vec[k], 
               route_vec[k],
               sender_in[k], sender_in_vld[k],
               &(sender_out[k]), &(sender_out_vld[k]));
        receiver(receiver_in[k], receiver_in_vld[k],
                 stream_out[k]);
        router(k,
               left_wires_rd[k], up_wires_rd[k], down_wires_rd[k], 
               left_vld_rd[k], up_vld_rd[k], down_vld_rd[k], 
               &(east_prev_win[k]), &(north_prev_win[k]), &(south_prev_win[k]),
               &(left_wires_wr[k]), &(right_wires_wr[k]), &(up_wires_wr[k]), &(down_wires_wr[k]), 
               &(left_vld_wr[k]), &(right_vld_wr[k]), &(up_vld_wr[k]), &(down_vld_wr[k]));
    }

    WIRES: for(int k=0; k<NUM_ROUTERS; k++) {
        left_wires_rd[k] = sender_out[k];
        left_vld_rd[k] = sender_out_vld[k];
        sender_in[k] = left_wires_wr[k];
        sender_in_vld[k] = left_vld_wr[k];
        receiver_in[k] = right_wires_wr[k];
        receiver_in_vld[k] = right_vld_wr[k];

        if (k == 0) {  // first router 
            //first router does not need up input/output
            down_wires_rd[k] = up_wires_wr[k+1];
            down_vld_rd[k] = up_vld_wr[k+1];
        } else if (k == NUM_ROUTERS-1){ // last router
            //last router does not need down input/output
            up_wires_rd[k] = down_wires_wr[k-1];
            up_vld_rd[k] = down_vld_wr[k-1];
        } else {
            up_wires_rd[k] = down_wires_wr[k-1];
            up_vld_rd[k] = down_vld_wr[k-1];
            down_wires_rd[k] = up_wires_wr[k+1];
            down_vld_rd[k] = up_vld_wr[k+1];
        }
    }

}
