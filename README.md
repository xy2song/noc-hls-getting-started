# noc-hls-getting-started

To compile single router HLS project, run:

`vivado_hls -f build_hls_router.tcl`

To compile 4-routers NoC HLS project, run:

`vivado_hls -f build_hls_ring_noc.tcl`
