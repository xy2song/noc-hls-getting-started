#include "/opt/Xilinx/Vivado/2018.3/include/gmp.h"  //cosim workaround for know Vivado HLS bug
#include "hls_stream.h"
#include "ap_int.h"
#include <stdio.h>
#include "top.h"

void receiver (NOC_PKT receiver_in, bool receiver_in_vld,
               STREAM_T &stream_out) {
    FIFO_T pkt_out;
    if (receiver_in_vld == 1){
        pkt_out.index = receiver_in.index;
        pkt_out.payload = receiver_in.payload;
        stream_out.write(pkt_out);
    } 
}
