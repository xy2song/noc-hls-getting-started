open_project noc_ring
set_top noc_ring
add_files noc_ring.cpp
add_files router.cpp
add_files sender.cpp
add_files receiver.cpp
add_files -tb noc_ring_tb_random.cpp
open_solution "solution0"
set_part {xc7z020clg400-1}

create_clock -period 3 -name default

csim_design 
csynth_design
cosim_design -rtl verilog -trace_level all
close_project

exit


