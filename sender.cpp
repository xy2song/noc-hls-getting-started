#include "/opt/Xilinx/Vivado/2018.3/include/gmp.h"  //cosim workaround for know Vivado HLS bug
#include "hls_stream.h"
#include "ap_int.h"
#include <stdio.h>
#include "top.h"

void sender (STREAM_T &stream_in, 
             ap_uint<MAX_NUM_BITS_VEC_INDEX> indir_vec[MAX_INDIR_VECTOR_SIZE], 
             ap_uint<NUM_BITS_ROUTER_ADDR> route_vec[MAX_INDIR_VECTOR_SIZE],
             NOC_PKT sender_in, bool sender_in_vld,
             NOC_PKT* sender_out, bool* sender_out_vld) {
    uint32_t in_vec_index;
    ap_uint<MAX_NUM_BITS_VEC_INDEX> out_vec_index;

    if (sender_in_vld == 1){
        *sender_out = sender_in; //backpressure from NoC. send back to NoC
        *sender_out_vld = 1;
    } 
    else {
        // temporary variables
        FIFO_T fifo_pkt_temp;
        NOC_PKT noc_temp;
        if (!stream_in.empty()){
            // read packet and store in temporary packet variable
            stream_in.read(fifo_pkt_temp);
            in_vec_index = fifo_pkt_temp.index;
            // construct output packet
            noc_temp.index = indir_vec[in_vec_index];
            noc_temp.payload = fifo_pkt_temp.payload;
            noc_temp.dst_router_addr = route_vec[in_vec_index];
            *sender_out = noc_temp;
            *sender_out_vld = 1;
        } else {
            *sender_out_vld = 0;
        } 
    }
}
