#include "/opt/Xilinx/Vivado/2018.3/include/gmp.h" //cosim workaround for know Vivado HLS bug
#include "hls_stream.h"
#include "ap_int.h"
#include <stdint.h>

#define NUM_RANDOM_PKTS 200

#define NUM_ROUTERS            4
#define MAX_NUM_BITS_VEC_INDEX 5
#define NUM_BITS_ROUTER_ADDR   2
#define NUM_BITS_PAYLOAD       32
#define NUM_BITS_ENSEMBLE_PKT  MAX_NUM_BITS_VEC_INDEX+NUM_BITS_PAYLOAD
#define NUM_BITS_NOC_PKT       MAX_NUM_BITS_VEC_INDEX+NUM_BITS_ROUTER_ADDR+NUM_BITS_PAYLOAD
#define TOTAL_INPUT_SIZE       11
#define MAX_INDIR_VECTOR_SIZE  32
#define INDIRECTION_VEC        {9, 5, 2, 1, 3, 4, 10, 7, 8, 0, 6}
#define ROUTING_VEC            {0, 3, 2, 2, 2, 3,  0, 3, 1, 3, 2}

#define NUM_BITS_NOC_PKT      MAX_NUM_BITS_VEC_INDEX+NUM_BITS_ROUTER_ADDR+NUM_BITS_PAYLOAD

//input directions
#define DOWN 0
#define UP   1
#define LEFT 2
#define DONT_CARE   0
//output directions
#define NO_REQ 0
#define EAST  1
#define NORTH 2
#define SOUTH 3

typedef float DATA_T;  // Data type used for math

struct FIFO_T {  //internal FIFO. No need for last signal since no DMA involved
    ap_uint<MAX_NUM_BITS_VEC_INDEX> index;
    DATA_T payload;
};

struct NOC_PKT {
    ap_uint<NUM_BITS_ROUTER_ADDR> dst_router_addr;
    ap_uint<MAX_NUM_BITS_VEC_INDEX> index;
    DATA_T payload;
};

typedef hls::stream<FIFO_T> STREAM_T;  

void noc_ring (STREAM_T stream_in[NUM_ROUTERS],
               STREAM_T stream_out[NUM_ROUTERS],
               ap_uint<MAX_NUM_BITS_VEC_INDEX> indir_vec[NUM_ROUTERS][MAX_INDIR_VECTOR_SIZE], 
               ap_uint<NUM_BITS_ROUTER_ADDR> route_vec[NUM_ROUTERS][MAX_INDIR_VECTOR_SIZE] );

void router (ap_uint<NUM_BITS_ROUTER_ADDR> router_addr,
             NOC_PKT left_in, NOC_PKT up_in, NOC_PKT down_in, 
             bool left_in_vld, bool up_in_vld, bool down_in_vld, 
             ap_uint<1>* east_prev_win, ap_uint<1>* north_prev_win, ap_uint<1>* south_prev_win,
             NOC_PKT* west_out, NOC_PKT* east_out, NOC_PKT* north_out, NOC_PKT* south_out, 
             bool* west_out_vld, bool* east_out_vld, bool* north_out_vld, bool* south_out_vld);

void sender (STREAM_T &stream_in, 
             ap_uint<MAX_NUM_BITS_VEC_INDEX> indir_vec[MAX_INDIR_VECTOR_SIZE], 
             ap_uint<NUM_BITS_ROUTER_ADDR> route_vec[MAX_INDIR_VECTOR_SIZE],
             NOC_PKT sender_in, bool sender_in_vld,
             NOC_PKT* sender_out, bool* sender_out_vld);

void receiver (NOC_PKT receiver_in, bool receiver_in_vld,
               STREAM_T &stream_out);
