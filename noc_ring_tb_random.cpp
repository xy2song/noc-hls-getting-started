#include "/opt/Xilinx/Vivado/2018.3/include/gmp.h"  //cosim workaround for know Vivado HLS bug
#include <stdio.h>
#include "top.h"
#include "ap_int.h"
#include <random>
#include <algorithm>
#include <vector>

/*
NoC testbench
Randomly generates NUM_RANDOM_PKTS packets and inject them into the NoC.
Works with any number of routers
*/

bool operator==(const FIFO_T& lhs, const FIFO_T& rhs) {
    return (lhs.index == rhs.index) && (lhs.payload == rhs.payload);
}

bool operator!=(const FIFO_T& lhs, const FIFO_T& rhs) {
    return (lhs.index != rhs.index) || (lhs.payload != rhs.payload);
}

int find_fifo_packet(std::vector<FIFO_T> vector, FIFO_T target, int array_size) {
    //check if target exists in vector
    //returns index of the target's first appearance in vector if found. Else return -1
    for (int i = 0; i < array_size; i++){
        if (vector[i] == target){
            return i;
        }
    }
    return -1;
}

int main() {
    STREAM_T stream_in[NUM_ROUTERS];  // one stream for each ensemble
    #pragma HLS stream variable=stream_in depth=40
    STREAM_T stream_out[NUM_ROUTERS];
    #pragma HLS stream variable=stream_out depth=40

    FIFO_T NULL_PACKET = {0xFFFFFFFFFF, 0}; //used for checking if a packet is received or not

    //random generator
    std::random_device rd; //obtain a random number from hardware
    //std::mt19937 gen(rd()); //seed the generator
    std::mt19937 gen(120); //fixed seed. SEED MUST BE FIXED FOR COSIM
    std::uniform_int_distribution<> stream_in_distr(0, NUM_ROUTERS-1); //randomize which stream_in to inject the packet
    std::uniform_int_distribution<> packet_distr(0, TOTAL_INPUT_SIZE-1); //randomize the packet's source index
    
    // To need to create 1 copy of indirection vector for each router.
    ap_uint<MAX_NUM_BITS_VEC_INDEX> indirection_vec[NUM_ROUTERS][MAX_INDIR_VECTOR_SIZE] = {INDIRECTION_VEC, INDIRECTION_VEC, INDIRECTION_VEC, INDIRECTION_VEC};
    ap_uint<NUM_BITS_ROUTER_ADDR> routing_vec[NUM_ROUTERS][MAX_INDIR_VECTOR_SIZE] = {ROUTING_VEC, ROUTING_VEC, ROUTING_VEC, ROUTING_VEC};

    //ap_uint<NUM_BITS_ENSEMBLE_PKT> src_pkts[NUM_RANDOM_PKTS]; 
    FIFO_T src_pkts[NUM_RANDOM_PKTS]; 
    ap_uint<MAX_NUM_BITS_VEC_INDEX> random_src_index;
    ap_uint<NUM_BITS_PAYLOAD> payload = 0;

    std::vector<FIFO_T> dst_pkts[NUM_ROUTERS]; //array of vectors containing expected destination packets

    int err_cnt = 0;
    //temporary variables
    ap_uint<NUM_BITS_ROUTER_ADDR> dst_router;
    ap_uint<MAX_NUM_BITS_VEC_INDEX> dst_index;

    for (int i = 0; i < NUM_RANDOM_PKTS; i++){
        //construct random input packets
        random_src_index = packet_distr(gen);
        src_pkts[i].index = random_src_index;
        src_pkts[i].payload = payload;
        //construct random dst_pkts
        dst_router = routing_vec[0][random_src_index]; //just use the first routing vector since they are all the same
        dst_index = indirection_vec[0][random_src_index];
        FIFO_T temp_dst_pkt;
        temp_dst_pkt.index = dst_index;
        temp_dst_pkt.payload = payload;
        std::cout << std::hex << "expecting index(" << dst_index << ")data(" << payload << ")"  << " at stream " << dst_router << std::endl;
        dst_pkts[dst_router].push_back(temp_dst_pkt);
        //each packet's payload increments by 1
        payload++;
    }

    //inject packets into the NoC
    for (int i = 0; i < NUM_RANDOM_PKTS; i++){
        int input_stream = stream_in_distr(gen); //inject into random input port
        stream_in[input_stream].write(src_pkts[i]);
        std::cout << std::hex << "sending packet: index(" << src_pkts[i].index << ")data(" << src_pkts[i].payload << ")"  << " at stream " << input_stream << std::endl;
    }
    
    //run the NoC enough times such that all packets have exited
    for (int i = 0; i < NUM_RANDOM_PKTS*3; i++){
        noc_ring(stream_in, stream_out, indirection_vec, routing_vec);
    }

    //temporary variables
    FIFO_T out_data;
    int location;
    //check whether each stream_out received the correct packets
    for (int j = 0; j < NUM_ROUTERS; j++){
        //read out all packets at each output port
        while (stream_out[j].read_nb(out_data)){
        //while (stream_out[j].read(out_data)){
            location = find_fifo_packet(dst_pkts[j], out_data, dst_pkts[j].size());
            if (location != -1) { 
                //received correct packet from NoC
                std::cout << std::hex << "Correctly received packet: " << dst_pkts[j][location].payload << " in stream_out[" << j << "]" << std::endl;
                dst_pkts[j][location] = NULL_PACKET; //marked as cleared
            } else { 
                //received unexpected packet from NoC
                //std::cout << std::hex << "ERROR! Received unexpected packet at stream_out[0]: index(" << out_data.axi_data.index << ")data(" << out_data.axi_data.payload << ")" << std::endl;
                std::cout << std::hex << "ERROR! Received unexpected packet at stream_out[0]: " << out_data.payload << std::endl;
                err_cnt++;
            }
        }
    }
    //check whether each stream_out received all expected packets
    for (int j = 0; j < NUM_ROUTERS; j++){
        for (int i = 0; i < dst_pkts[j].size(); i++){
            if (dst_pkts[j][i] != NULL_PACKET) { //packet not marked as cleared means packet was not received
                std::cout << std::hex << "ERROR! Did not receive expected packet " << dst_pkts[j][i].payload << " at stream_out[" << j << "]" << std::endl;
                err_cnt++;
            }
        }
    }
    //result message
    std::cout << "*** *** *** *** ***" << std::endl;
    if (err_cnt) {
        std::cout << std::dec << "!!! TESTS FAILED - " << err_cnt << " errors detected !!!" << std::endl;
    } else {
        std::cout << "TESTS PASSED" << std::endl;
    }
    std::cout << "*** *** *** *** ***" << std::endl;
    // Only return 0 on success
    return err_cnt;
}

